# IOC Metadata EPICS Module
This module provides a way to expose [IOC database info tags][1] through PV Access network. 

## Overview
iocMetadata is an EPICS IOC support library that provides two functions that create read-only NTTable EPICS PVs whose value is defined by specific info tags present in the IOC database.

The first command is the `pvlistFromInfo`, which generates a NTTable PV with a single column and lines filled with all EPICS record names that contains a specific info tag.

The second command is the `iocMetadataConfigure` that generates a NTTable PV of three columns with all the info tags keys, tags values and record names they are associated. 

These NTTable PVs can then be read by any EPICS 7 client on the network.

## Dependencies
- EPICS Base > 7.0

## Usage

### Example of use case

This module can be used, for instance, as a helper to create a list of the IOC PVs that should be archived or added to a Save & Restore list.

### pvlistFromInfo - List records that contains an specific info tag

```bash
pvlistFromInfo("info_tag_name", "resulting_nttable_pv_name")
```

The function call must provide two arguments:
- info tag name: String that will be used to search for valid info tags. This string must be **exact the same** as the info tag being searched;
- resulting NTTable name: This is the prefix name for the NTTable PV that will be generated.

### Example

If a given IOC contains the following info tags for two of its records that, e.g., contain PVs to be archived:

```
record(ai, "$(PREFIX)SignalA") {
    field(DESC, "Simple analog input")
    info(ARCHIVE_THIS, "VAL EGU")
}

record(ai, "$(PREFIX)SignalB") {
    field(DESC, "Simple analog input")
    info(SAVERESTORE_THIS, "")
}
```

Then the following snippet will generate the NTTable PVs:

```
require iocmetadata

epicsEnvSet(PREFIX, "EXAMPLE")
dbLoadRecords("$(E3_CMD_TOP)/example.template", "PREFIX=$(PREFIX)-A:")
dbLoadRecords("$(E3_CMD_TOP)/example.template", "PREFIX=$(PREFIX)-B:")
dbLoadRecords("$(E3_CMD_TOP)/example.template", "PREFIX=$(PREFIX)-C:")
dbLoadRecords("$(E3_CMD_TOP)/example.template", "PREFIX=$(PREFIX)-D:")

pvlistFromInfo("ARCHIVE_THIS", "$(PREFIX):ArchiverList")
pvlistFromInfo("SAVERESTORE_THIS", "$(PREFIX):SaveRestoreList")

iocInit()
```

Two special NTTable PVs are created:
- `EXAMPLE:ArchiverList`
- `EXAMPLE:SaveRestoreList`

Any PVA client can access the value of these PVs:

```
$> pvget EXAMPLE:ArchiverList
EXAMPLE:ArchiverList epics:nt/NTTable:1.0
    string[] pvlist [EXAMPLE-A:SignalA.VAL, EXAMPLE-A:SignalA.EGU, EXAMPLE-B:SignalA.VAL, EXAMPLE-B:SignalA.EGU, EXAMPLE-C:SignalA.VAL, EXAMPLE-C:SignalA.EGU, EXAMPLE-D:SignalA.VAL, EXAMPLE-D:SignalA.EGU]

$> pvget EXAMPLE:SaveRestoreList
EXAMPLE:SaveRestoreList epics:nt/NTTable:1.0
    string[] pvlist [EXAMPLE-A:SignalB, EXAMPLE-B:SignalB, EXAMPLE-C:SignalB, EXAMPLE-D:SignalB]
```

### iocMetadataConfigure - Create tables

```bash
iocMetadataConfigure("matching_string","PV_prefix")
```

The function call must provide two arguments:
- Matching string: String that will be used to search for valid info tags. All tags starting with "matching string" will be included on the NTTable PV;
- PV prefix: This is the prefix name for the NTTable PV. The final name will be `PREFIX:infoTagsMetadata`.

### Example

If a given IOC contains the following info tags for two of its records:

```bash
# example.template:

record(ao, "IOC_RECORD_A") {
    field(DESC, "Simple analog output")
    info(TAG_NAME_1, "RECORD_A_TAG_1")
    info(TAG_NAME_2, "RECORD_A_TAG_2")
}

record(ao, "IOC_RECORD_B") {
    field(DESC, "Simple analog output")
    info(TAG_NAME_1, "RECORD_B_TAG_1")
    info(TAG_NAME_2, "RECORD_B_TAG_2")
}
```

Then the following snippet will generate a NTTable PV named `IOC_TAGS:infoTagsMetadata`:

```bash
# st.cmd:

...

dbLoadRecords("example.template")
iocMetadataConfigure "TAG_NAME" "IOC_TAGS"

...

iocInit()

```

A client with EPICS 7 tools can get the value of the newly generated NTTable PV:

```bash
$> pvget IOC_TAGS:infoTagsMetadata

IOC_TAGS:infoTagsMetadata
"Info Tag Names" "Info Tag Values" "Record Names"
   TAG_NAME_1      RECORD_A_TAG_1      IOC_RECORD_A
   TAG_NAME_2      RECORD_A_TAG_2      IOC_RECORD_A
   TAG_NAME_1      RECORD_B_TAG_1      IOC_RECORD_B
   TAG_NAME_2      RECORD_B_TAG_2      IOC_RECORD_B

$> pvinfo IOC_TAGS:infoTagsMetadata

IOC_TAGS:infoTagsMetadata
Server: 127.0.0.1:5075
Type:
    epics:nt/NTTable:1.0
        string[] labels
        structure value
            string[] key
            string[] value
            string[] recordname

```

## Including iocMetadata in your EPICS IOC application

Add iocMetadata to the application configure/RELEASE or RELEASE.local file.

```bash
cat <<EOF > configure/RELEASE.local
IOCMETADATA=/path/to/your/build/of/iocMetadata
EOF
```

Then add the iocMetadata library as a dependency to your executable or library. EPICS 7 libraries must also be listed, including QSRV and pvDatabase:

```bash
PROD_IOC += myioc
DBD += myioc.dbd
...

# myioc.dbd will be made up from these files:
myioc_DBD += base.dbd
myioc_DBD += iocMetadata.dbd
myioc_DBD += PVAServerRegister.dbd
myioc_DBD += qsrv.dbd
...

# Add all the support libraries needed by this IOC
myioc_LIBS += iocMetadata
myioc_LIBS += qsrv pvDatabase
myioc_LIBS += $(EPICS_BASE_PVA_CORE_LIBS)
...

```

### References

[1]: EPICS Application Developer's Guide: https://docs.epics-controls.org/en/latest/software/base.html
