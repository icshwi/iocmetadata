/**
 * @file pvListCollector.cpp
 * @author Joao Paulo Martins (joaopaulo.martins@ess.eu)
 * @brief 
 * @version 0.1
 * @date 2021-08-05
 * 
 * @copyright Copyright (c) 2021 European Spallation Source ERIC
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <memory>
#include <sstream>
#include <algorithm>
#include <iterator>

#include <pv/pvDatabase.h>
#include <pv/channelProviderLocal.h>

#include	<dbStaticLib.h>
#include	<dbAccess.h>  /* includes dbDefs.h, dbBase.h, dbAddr.h, dbFldTypes.h */
#include	<recSup.h>  /* rset */
#include	<dbConvert.h>  /* dbPutConvertRoutine */
#include	<dbConvertFast.h>  /* dbFastPutConvertRoutine */
#include	<initHooks.h>
#include	<epicsThread.h>
#include	<errlog.h>
#include	<iocsh.h>
#include	<special.h>
#include	<macLib.h>
#include	<epicsString.h>
#include	<dbAccessDefs.h>
#include	<epicsStdio.h>

#include <iocsh.h>
#include <initHooks.h>

// The following must be the last include for code database uses
#include <epicsExport.h>

#define epicsExportSharedSymbols
#include "pvListRec.h"

using namespace std;
using namespace epics::pvData;
using namespace epics::pvDatabase;

/* Static variables that capture the state of this feature */
vector<pair<string, string> > pvListInputTable;
bool configurePvListCalled = false;
vector<string> nttablePvNames;

int databaseParser(const string & matchingStr, pvListRecordType & table)
{
	DBENTRY dbentry;
	DBENTRY *pdbentry = &dbentry;

    string info_value;
    string info_tag;
    string pv_name;

	int	searchRecord;
    int addedPvs = 0;

	if (!pdbbase) {
		errlogPrintf("pvlistFromInfo: No Database Loaded\n");
		return 0;
	}

    if (matchingStr.empty()) {
		errlogPrintf("pvlistFromInfo: info tag matching string not informed\n");
		return 0;
    }

	dbInitEntry(pdbbase,pdbentry);
	/* loop over all record types */
	dbFirstRecordType(pdbentry);
	do {
		/* loop over all records of current type*/
		dbFirstRecord(pdbentry);
		searchRecord = dbIsAlias(pdbentry) ? 0 : 1;
		do {
			if (searchRecord) {
				/* try to find first INFO tag */
                if (dbFirstInfo(pdbentry) == 0) {
                    do {
                        info_tag = dbGetInfoName(pdbentry);
                        info_value = dbGetInfoString(pdbentry);
                        pv_name = dbGetRecordName(pdbentry);

                        // Go through the info tag value and extract PV names to be added to the list
                        if (info_tag.compare(0, matchingStr.size(), matchingStr) == 0) {

                            // Splitting field values
                            std::istringstream iss(info_value);
                            std::vector<std::string> fields;
                            std::copy(std::istream_iterator<std::string>(iss),
                                std::istream_iterator<std::string>(),
                                std::back_inserter(fields));

                            // Check if fields exists before adding them to the list
                            for (auto fld : fields) {
                                if (dbFindField(pdbentry, fld.c_str()) == 0) {
                                    table.push_back(std::string(pv_name+"."+fld));
                                    ++addedPvs;
                                }
                            }

                            // If value is empty, just add regular PV name (VAL)
                            if (fields.size() == 0) {
                                table.push_back(pv_name);
                                ++addedPvs;
                            }
                        }
                    } while (dbNextInfo(pdbentry) == 0);
                }
            }
		} while (dbNextRecord(pdbentry) == 0);
	} while (dbNextRecordType(pdbentry) == 0);
	dbFinishEntry(pdbentry);
	return addedPvs;
}


void pvListCollectorHooks(initHookState state)
{
    if(state==initHookAfterIocRunning) {
        if (pvListInputTable.size() > 0) {
            for (size_t i = 0; i < pvListInputTable.size(); i++) {
                // Check if NTTable with the same name was already created
                string ntTableRecordName = pvListInputTable[i].second;
                for (auto ntt_name : nttablePvNames) {
                    if (ntt_name == ntTableRecordName) {
                        errlogPrintf("pvlistFromInfo: Name %s was already used. Ignoring %s info tag\n", ntTableRecordName.c_str(), pvListInputTable[i].first.c_str());
                        return;
                    }
                }

                // Create record with NTTable
                pvListRecordPtr record = pvListRecord::create(ntTableRecordName);

                /* Populate simple 1-column table with PV names that contain the informed info tag */
                if (databaseParser(pvListInputTable[i].first, record->getTableRef())) {
                    record->updateTable();
                    bool result = PVDatabase::getMaster()->addRecord(record);
                    if(!result)
                        errlogPrintf("pvlistFromInfo: NTTable record %s was not added to the IOC\n", ntTableRecordName.c_str());
                    else {
                        errlogPrintf("pvlistFromInfo: NTTable record %s was added to the IOC\n", ntTableRecordName.c_str());
                        nttablePvNames.push_back(std::string(ntTableRecordName));
                    }
                } else {
                    errlogPrintf("pvlistFromInfo: No pvs were found with the %s info tag\n", pvListInputTable[i].first.c_str());
                }
            }
        }
        configurePvListCalled = true;
    }
}

static const iocshArg funcArg0 = { "Matching String", iocshArgString };
static const iocshArg funcArg1 = { "NTTable PV Name", iocshArgString };
static const iocshArg *funcArgs[] = {&funcArg0, &funcArg1};
static const iocshFuncDef pvListCollectorFuncDef = {"pvlistFromInfo", 2, funcArgs};

static void pvListCollectorCallFunc(const iocshArgBuf *args)
{
    if (!configurePvListCalled) {
        if(!args[0].sval || !args[1].sval) {
            throw std::runtime_error("pvlistFromInfo: missing arguments. Usage: pvlistFromInfo [INFOTAG_STRING] [RECORD FIELDS (VAL, HIHI, etc..)]");
        }

        /* Sets the matching string and PV name according to the arguments passed */
        pair<string, string> tmp(args[0].sval, args[1].sval);
        pvListInputTable.push_back(tmp);
    }
    else
    {
        errlogPrintf("pvlistFromInfo: IOC already running, this function call causes no effect\n");
    }

}

static void pvListCollectorRegister(void)
{
    static int firstTime = 1;
    if (firstTime) {
        configurePvListCalled = false;
        iocshRegister(&pvListCollectorFuncDef, pvListCollectorCallFunc);
        initHookRegister(&pvListCollectorHooks);
    }
}

extern "C" {
    epicsExportRegistrar(pvListCollectorRegister);
}
