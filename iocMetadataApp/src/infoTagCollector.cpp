/**
 * @file infoTagCollector.cpp
 * @author Joao Paulo Martins (joaopaulo.martins@ess.eu)
 * @brief 
 * @version 0.1
 * @date 2020-05-20
 * 
 * @copyright Copyright (c) 2020 European Spallation Source ERIC
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <iostream>
#include <string>
#include <vector>
#include <utility>

#include <pv/pvDatabase.h>
#include <pv/channelProviderLocal.h>

#include	<dbStaticLib.h>
#include	<dbAccess.h>  /* includes dbDefs.h, dbBase.h, dbAddr.h, dbFldTypes.h */
#include	<recSup.h>  /* rset */
#include	<dbConvert.h>  /* dbPutConvertRoutine */
#include	<dbConvertFast.h>  /* dbFastPutConvertRoutine */
#include	<initHooks.h>
#include	<epicsThread.h>
#include	<errlog.h>
#include	<iocsh.h>
#include	<special.h>
#include	<macLib.h>
#include	<epicsString.h>
#include	<dbAccessDefs.h>
#include	<epicsStdio.h>

#include <iocsh.h>
#include <initHooks.h>

// The following must be the last include for code database uses
#include <epicsExport.h>

#define epicsExportSharedSymbols
#include "infoTableRec.h"

using namespace std;
using namespace epics::pvData;
using namespace epics::pvDatabase;


/* Static variables that capture the state of this feature */
vector<pair<string, string> > inputTable;
bool configureFunctionCalled = false;

void createManagerData(const string & matchingStr, infoTableRecordType & table)
{
	DBENTRY dbentry;
	DBENTRY *pdbentry = &dbentry;

    string info_value;
    string info_tag;
    string pv_name;

	int searchRecord;

	if (!pdbbase) {
		errlogPrintf("iocMetadataConfigure: No Database Loaded\n");
		return;
	}

    if (matchingStr.empty()) {
		errlogPrintf("iocMetadataConfigure: info tag matching string not informed\n");
		return;
    }

	dbInitEntry(pdbbase,pdbentry);
	/* loop over all record types */
	dbFirstRecordType(pdbentry);
	do {
		/* loop over all records of current type*/
		dbFirstRecord(pdbentry);
		searchRecord = dbIsAlias(pdbentry) ? 0 : 1;
		do {
			if (searchRecord) {
				/* try to find first INFO tag */
                if (dbFirstInfo(pdbentry) == 0) {
                    do {
                        info_tag = dbGetInfoName(pdbentry);
                        info_value = dbGetInfoString(pdbentry);
                        pv_name = dbGetRecordName(pdbentry);

                        // if the tag name matches the standard, save [TAG, VALUE, PVNAME] into table
                        // TODO: use regex
                        if (info_tag.compare(0, matchingStr.size(), matchingStr) == 0) {
                            vector<string> line;
                            line.push_back(info_tag);
                            line.push_back(info_value);
                            line.push_back(pv_name);

                            table.push_back(line);
                        }
                    } while (dbNextInfo(pdbentry) == 0);
                }
            }
		} while (dbNextRecord(pdbentry) == 0);
	} while (dbNextRecordType(pdbentry) == 0);
	dbFinishEntry(pdbentry);
	return;
}


void infoTagCollectorHooks(initHookState state)
{
    if(state==initHookAfterIocRunning) {
        if (inputTable.size() > 0) {
            for (size_t i = 0; i < inputTable.size(); i++) {

                /* Create record with NTTable */
                string ntTableRecordName = inputTable[i].second + ":infoTagsMetadata";
                infoTableRecordPtr record = infoTableRecord::create(ntTableRecordName);

                /* Populate simple 3-column table with info tag information */
                createManagerData(inputTable[i].first, record->getTableRef());
                record->updateTable();

                bool result = PVDatabase::getMaster()->addRecord(record);
                if(!result)
                    errlogPrintf("iocMetadataConfigure: NTTable record %s was not added to the IOC\n", ntTableRecordName.c_str());
            }
        }
        configureFunctionCalled = true;
    }
}

static const iocshArg funcArg0 = { "Matching String", iocshArgString };
static const iocshArg funcArg1 = { "NTTable PV Name", iocshArgString };
static const iocshArg *funcArgs[] = {&funcArg0, &funcArg1};
static const iocshFuncDef infoTagCollectorFuncDef = {"iocMetadataConfigure", 2, funcArgs};

static void infoTagCollectorCallFunc(const iocshArgBuf *args)
{
    if (!configureFunctionCalled) {
        if(!args[0].sval || !args[1].sval) {
            throw std::runtime_error("iocMetadataConfigure: missing arguments. Usage: iocMetadataConfigure [INFOTAG_STRING] [NTTABLE_NAME]");
        }

        /* Sets the matching string and PV name according to the arguments passed */
        pair<string, string> tmp(args[0].sval, args[1].sval);
        inputTable.push_back(tmp);
    }
    else
    {
        errlogPrintf("iocMetadataConfigure: IOC already running, this function call causes no effect\n");
    }

}

static void infoTagCollectorRegister(void)
{
    static int firstTime = 1;
    if (firstTime) {
        configureFunctionCalled = false;
        iocshRegister(&infoTagCollectorFuncDef, infoTagCollectorCallFunc);
        initHookRegister(&infoTagCollectorHooks);
    }
}

extern "C" {
    epicsExportRegistrar(infoTagCollectorRegister);
}
