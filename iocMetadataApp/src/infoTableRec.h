/**
 * @file infoTableRec.h
 * @author Joao Paulo Martins (joaopaulo.martins@ess.eu)
 * @brief 
 * @version 0.1
 * @date 2020-05-20
 * 
 * @copyright Copyright (c) 2020 European Spallation Source ERIC
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _INFOTABLE_REC_H_
#define _INFOTABLE_REC_H_

#include <vector>
#include <string>

#include <pv/pvDatabase.h>
#include <pv/timeStamp.h>
#include <pv/pvTimeStamp.h>

#include <shareLib.h>

class infoTableRecord;
typedef std::tr1::shared_ptr<infoTableRecord> infoTableRecordPtr;
typedef std::vector<std::vector<std::string> > infoTableRecordType;

/**
 * @brief infoTable Record Class Definition
 * 
 */
class epicsShareClass infoTableRecord :
    public epics::pvDatabase::PVRecord
{
public:
    POINTER_DEFINITIONS(infoTableRecord);
    static infoTableRecordPtr create(std::string const & recordName);
    virtual void process();

    virtual ~infoTableRecord() {}
    virtual bool init() {return false;}
    infoTableRecordType& getTableRef() {return rawData; }
    void updateTable();

private:
    infoTableRecord(std::string const & recordName,
        epics::pvData::PVStructurePtr const & pvStructure);

    infoTableRecordType rawData;

};

#endif  /* _INFOTABLE_REC_H_ */
