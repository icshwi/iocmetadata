/**
 * @file infoTableRec.cpp
 * @author Joao Paulo Martins (joaopaulo.martins@ess.eu)
 * @brief 
 * @version 0.1
 * @date 2020-05-20
 * 
 * @copyright Copyright (c) 2020 European Spallation Source ERIC
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <cmath>
#include <pv/pvDatabase.h>
#include <pv/standardField.h>

#define epicsExportSharedSymbols
#include "infoTableRec.h"

using namespace epics::pvData;
using namespace epics::pvDatabase;
namespace pvd = epics::pvData;
using std::tr1::static_pointer_cast;
using std::string;
using std::vector;
using std::cout;

infoTableRecordPtr infoTableRecord::create(string const & recordName)
{
    StandardFieldPtr standardField = getStandardField();
    FieldCreatePtr fieldCreate = getFieldCreate();
    PVDataCreatePtr pvDataCreate = getPVDataCreate();

    // Standard NTTable record with 3 columns
    StructureConstPtr topStructure(getFieldCreate()->createFieldBuilder()
                                ->setId("epics:nt/NTTable:1.0")
                                ->addArray("labels", pvd::pvString)
                                ->addNestedStructure("value")
                                    ->addArray("key", pvd::pvString)
                                    ->addArray("value", pvd::pvString)
                                    ->addArray("recordname", pvd::pvString)
                                ->endNested()
                                ->createStructure());

    PVStructurePtr pvStructure = pvDataCreate->createPVStructure(topStructure);
    infoTableRecordPtr pvRecord(new infoTableRecord(recordName,pvStructure));

    pvRecord->initPVRecord();
    return pvRecord;
}

infoTableRecord::infoTableRecord(
    string const & recordName,
    PVStructurePtr const & pvStructure)
: PVRecord(recordName,pvStructure)
{}

/**
 * @brief Fill the "value" field of the NTTable with valid information
 * 
 */
void infoTableRecord::updateTable() {
    shared_vector<string> tagNamesArray;
    shared_vector<string> tagValuesArray;
    shared_vector<string> recNamesArray;
    shared_vector<string> labelsArray;

    /* Validate the table structure */
    if (rawData.size() == 0) {
        return;
    }

    bool isValid = true;
    for (size_t j = 0; j < rawData.size(); j++) {
        if (rawData[j].size() != 3) {
            isValid = false;
            break;
        }
    }

    if (!isValid)
        return;

    /* Insert the column labels strings */
    labelsArray.push_back("Info Tag Names");
    labelsArray.push_back("Info Tag Values");
    labelsArray.push_back("Record Names");

    /* Iterate through table and fill vectors */
    for (size_t j = 0; j < rawData.size(); j++) {
        tagNamesArray.push_back(rawData[j][0]);
        tagValuesArray.push_back(rawData[j][1]);
        recNamesArray.push_back(rawData[j][2]);
    }

    /* Transfer labels array to record field */
    PVStringArrayPtr labelsValue = getPVStructure()->getSubField<PVStringArray>("labels");
    labelsValue->putFrom(freeze(labelsArray));

    /* Transfer the info tag keys to its proper column */
    PVStringArrayPtr itValue = getPVStructure()->getSubField<PVStringArray>("value.key");
    itValue->putFrom(freeze(tagNamesArray));

    /* Transfer the info tag values to its proper column */
    PVStringArrayPtr ivValue = getPVStructure()->getSubField<PVStringArray>("value.value");
    ivValue->putFrom(freeze(tagValuesArray));

    /* Transfer the record names corresponding to the info tags to its proper column */
    PVStringArrayPtr rnValue = getPVStructure()->getSubField<PVStringArray>("value.recordname");
    rnValue->putFrom(freeze(recNamesArray));
}

void infoTableRecord::process()
{
    // prevent any external "put" operation to change the value of the record
    updateTable();
}


