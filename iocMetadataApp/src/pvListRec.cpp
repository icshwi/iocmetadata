/**
 * @file pvListRec.cpp
 * @author Joao Paulo Martins (joaopaulo.martins@ess.eu)
 * @brief 
 * @version 0.1
 * @date 2020-05-20
 * 
 * @copyright Copyright (c) 2020 European Spallation Source ERIC
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <cmath>
#include <pv/pvDatabase.h>
#include <pv/standardField.h>

#define epicsExportSharedSymbols
#include "pvListRec.h"

using namespace epics::pvData;
using namespace epics::pvDatabase;
namespace pvd = epics::pvData;
using std::tr1::static_pointer_cast;
using std::string;
using std::vector;
using std::cout;

pvListRecordPtr pvListRecord::create(string const & recordName)
{
    StandardFieldPtr standardField = getStandardField();
    FieldCreatePtr fieldCreate = getFieldCreate();
    PVDataCreatePtr pvDataCreate = getPVDataCreate();

    // Standard NTTable record with 3 columns
    StructureConstPtr topStructure(getFieldCreate()->createFieldBuilder()
                                ->setId("epics:nt/NTTable:1.0")
                                ->addArray("pvlist", pvd::pvString)
                                ->createStructure());

    PVStructurePtr pvStructure = pvDataCreate->createPVStructure(topStructure);
    pvListRecordPtr pvRecord(new pvListRecord(recordName,pvStructure));
    
    pvRecord->initPVRecord();
    return pvRecord;
}

pvListRecord::pvListRecord(
    string const & recordName,
    PVStructurePtr const & pvStructure)
: PVRecord(recordName,pvStructure)
{}

/**
 * @brief Fill the "value" field of the NTTable with valid information
 * 
 */
void pvListRecord::updateTable() {
    shared_vector<string> recNamesArray;

    /* Validate the table structure */
    if (rawData.size() == 0) {
        return;
    }

    /* Iterate through table and fill vectors */
    for (size_t j = 0; j < rawData.size(); j++) {
        recNamesArray.push_back(rawData[j]);
    }

    /* Transfer the record names corresponding to the info tags to its proper column */
    PVStringArrayPtr rnValue = getPVStructure()->getSubField<PVStringArray>("pvlist");
    rnValue->putFrom(freeze(recNamesArray));
}

void pvListRecord::process()
{
    // prevent any external "put" operation to change the value of the record
    updateTable();
}


