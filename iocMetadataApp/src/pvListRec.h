/**
 * @file pvListRec.h
 * @author Joao Paulo Martins (joaopaulo.martins@ess.eu)
 * @brief 
 * @version 0.1
 * @date 2021-08-04
 * 
 * @copyright Copyright (c) 2020 European Spallation Source ERIC
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _PVLIST_REC_H_
#define _PVLIST_REC_H_

#include <vector>
#include <string>

#include <pv/pvDatabase.h>
#include <pv/timeStamp.h>
#include <pv/pvTimeStamp.h>

#include <shareLib.h>

class pvListRecord;
typedef std::tr1::shared_ptr<pvListRecord> pvListRecordPtr;
typedef std::vector<std::string> pvListRecordType;

/**
 * @brief infoTable Record Class Definition
 * 
 */
class epicsShareClass pvListRecord :
    public epics::pvDatabase::PVRecord
{
public:
    POINTER_DEFINITIONS(pvListRecord);
    static pvListRecordPtr create(std::string const & recordName);
    virtual void process();

    virtual ~pvListRecord() {}
    virtual bool init() {return false;}
    pvListRecordType& getTableRef() {return rawData; }
    void updateTable();

private:
    pvListRecord(std::string const & recordName,
        epics::pvData::PVStructurePtr const & pvStructure);

    pvListRecordType rawData;

};

#endif  /* _PVLIST_REC_H_ */
