#!../../bin/linux-x86_64/iocMetadataTest

## Register all support components
dbLoadDatabase("../../dbd/iocMetadataTest.dbd",0,0)
iocMetadataTest_registerRecordDeviceDriver(pdbbase) 

dbLoadRecords("../../db/counters.template", "P=IOC2")
dbLoadRecords("../../db/infotags.template", "ITAG=BPMM, P=IOC2, N=1, M=1")
iocMetadataConfigure("BPMM", "IOC2")

iocInit()


