#!../../bin/linux-x86_64/iocMetadataTest

## Register all support components
dbLoadDatabase("../../dbd/iocMetadataTest.dbd",0,0)
iocMetadataTest_registerRecordDeviceDriver(pdbbase) 

dbLoadRecords("../../db/counters.template", "P=IOC3")
dbLoadRecords("../../db/infotags.template", "ITAG=BPMM, P=IOC3, N=2, M=2")
iocMetadataConfigure("BPMM", "IOC3")

iocInit()


