#!../../bin/linux-x86_64/iocMetadataTest

## Register all support components
dbLoadDatabase("../../dbd/iocMetadataTest.dbd",0,0)
iocMetadataTest_registerRecordDeviceDriver(pdbbase) 

dbLoadRecords("../../db/counters.template", "P=IOC1")
dbLoadRecords("../../db/infotags.template", "ITAG=BPMM, P=IOC1, N=0, M=0")
iocMetadataConfigure("BPMM", "IOC1")

iocInit()

